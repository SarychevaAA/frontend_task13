window.addEventListener('DOMContentLoaded', ()=>{
    const tabs = document.querySelectorAll('.tabheader__item');
    const tabsContent = document.querySelectorAll('.tabcontent');
    const tabsParent = document.querySelector('.tabheader__items');

    const closeTabContent = ()=>{
        tabsContent.forEach(item=>{
            item.style.display = 'none';
            item.classList.remove('show');
        });
        tabs.forEach(item=>{
            item.classList.remove('tabheader__item_active');
        });
    }

    const showTabContent = (index = 0)=>{
        tabsContent[index].style.display = 'block';
        tabsContent[index].classList.remove('hide');
        tabs[index].classList.add('tabheader__item_active');
    };
    closeTabContent();
    showTabContent();

    tabsParent.addEventListener('click', (event)=>{
        const target = event.target;
        if( target && target.classList.contains('tabheader__item')){
            tabs.forEach((item, index)=>{
                if (target == item){
                    closeTabContent();
                    showTabContent(index);
                }
            })
        }
    })


    const deadline = '2023-06-15';
    function getTimerCounting(endtime){
        const total = Date.parse(endtime) - Date.parse(new Date()),
            days = Math.floor(total/(1000 * 60 * 60 *24)),
            hours = Math.floor((total/(1000 * 60 * 60)) % 24),
            minutes = Math.floor((total /1000 / 60) % 60),
            seconds = Math.floor((total / 1000) % 60);

        return {
            total,
            days,
            hours,
            minutes,
            seconds
        }
    }

    function refactorTime(num){
        if (num >= 0 && num < 10){
            return `0${num}`;
        }
        else {
            return num;
        }
    }
    function changeClock(timerTag, endtime){
        const timer = document.querySelector(timerTag),
            days = timer.querySelector('#days'),
            hours = timer.querySelector('#hours'),
            minutes = timer.querySelector('#minutes'),
            seconds = timer.querySelector('#seconds'),
            timeInterval = setInterval(updateClock, 1000);

        updateClock();

        function updateClock() {
            const t = getTimerCounting(endtime);

            days.innerHTML = refactorTime(t.days);
            hours.innerHTML = refactorTime(t.hours);
            minutes.innerHTML = refactorTime(t.minutes);
            seconds.innerHTML = refactorTime(t.seconds);

            if (t.total <= 0){
                clearInterval(timeInterval);
            }
        }
    }
    changeClock('.timer', deadline);


    const modalOpenBtns = document.querySelectorAll('[data-modal]'),
        modal = document.querySelector('.modal'),
        modalCloseBtn = document.querySelector('[data-close]');

    function openModal(){
        modal.classList.add('show');
        modal.classList.remove('hide');
        document.body.style.overflow = 'hidden';
        clearInterval(modalTimerId);
    }
    modalOpenBtns.forEach(item=>{
        item.addEventListener('click', ()=>{
            openModal();
        })
    })

    function closeModal(){
        modal.classList.add('hide');
        modal.classList.remove('show');
        document.body.style.overflow = '';
    }
    modalCloseBtn.addEventListener('click', closeModal);

    modal.addEventListener('click', (event)=>{
        if (event.target === modal){
            closeModal();
        }
    })
    document.addEventListener('keydown', (event)=>{
        if (event.code === 'Escape'){
            closeModal();
        }
    })

    const modalTimerId = setTimeout(openModal, 5000);
    function showModalByScroll(){
        if (window.pageYOffset + document.documentElement.clientHeight >= document.documentElement.scrollHeight){
            openModal();
            window.removeEventListener('scroll', showModalByScroll);
        }
    }
    window.addEventListener('scroll', showModalByScroll);

    class MenuCard{
        constructor(src, alt, title, description, price, parentSelector, ...classes) {
            this.src = src;
            this.alt = alt;
            this.title = title;
            this.description = description;
            this.parent = document.querySelector(parentSelector);
            this.classes = classes || 'menu__item';
            this.price = price;
            this.transfer = 27;
            this.changeToUAH();
        }

        changeToUAH(){
            this.price = this.price * this.transfer;
        }

        render(){
            const element = document.createElement('div');

            if (this.classes.length === 0){
                this.element = 'menu__item';
                element.classList.add(this.element);
            }
            else{
                this.classes.forEach(className => element.classList.add(className));
            }
            element.innerHTML = `
                    <img src=${this.src} alt=${this.alt}>
                    <h3 class="menu__item-subtitle">${this.title}</h3>
                    <div class="menu__item-descr">${this.description}</div>
                    <div class="menu__item-divider"></div>
                    <div class="menu__item-price">
                        <div class="menu__item-cost">Цена:</div>
                        <div class="menu__item-total"><span>${this.price}</span> грн/день</div>
                    </div>
            `;
            this.parent.append(element);
        }
    }
    new MenuCard(
        "img/tabs/vegy.jpg",
        "vegy",
        'Меню "Фитнес"',
        "Меню \"Фитнес\" - это новый подход к приготовлению блюд: больше свежих овощей и фруктов. Продукт активных и здоровых людей. Это абсолютно новый продукт с оптимальной ценой и высоким качеством!",
        9,
        '.menu .container'
    ).render();

    new MenuCard(
        "img/tabs/elite.jpg",
        "elite",
        'Меню “Премиум”',
        "В меню “Премиум” мы используем не только красивый дизайн упаковки, но и качественное исполнение блюд. Красная рыба, морепродукты, фрукты - ресторанное меню без похода в ресторан!",
        21,
        '.menu .container'
    ).render();

    new MenuCard(
        "img/tabs/post.jpg",
        "post",
        'Меню "Постное"',
        "Меню “Постное” - это тщательный подбор ингредиентов: полное отсутствие продуктов животного происхождения, молоко из миндаля, овса, кокоса или гречки, правильное количество белков за счет тофу и импортных вегетарианских стейков. ",
        16 ,
        '.menu .container'
    ).render();
})
